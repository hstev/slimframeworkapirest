<?php
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/',
    function () 
	{
        //echo 12345;
        Functions::registry("/",http_response_code(),__FUNCTION__);
    }
);


require("entidades/db.php");
require("entidades/functions.php");

require("entidades/users/users.php");
require("entidades/siar/siar.php");
require("entidades/aemet/aemet.php");
require("entidades/energygest/energygest.php");

$app->run();



