<?php

function all()  {
   $post = json_decode($request->getBody());
   $valor['valor'] = formatNumber($post->number);
   generateJson($valor);
}

function getId()  {
   $valor['valor'] = formatNumber(4000000);
   generateJson($valor);
} 

function create()  {

	$table = 'users';
	
	$fields[] = 'nickname';
	$fields[] = 'fullname';
	$fields[] = 'email';
	$fields[] = 'active';
	
	$values[] = Functions::request('nickname');
	$values[] = Functions::request('fullname');
	$values[] = Functions::request('email');
	$values[] = 1;
	Functions::insert($table, $fields, $values);
	$resault['msg']	= "Datos almacenados";
	Functions::generateJson($resault);
}

function update()  {
   $valor['valor'] = Functions::formatNumber(4000000);
   Functions::generateJson($valor);
} 


$app->post('/users/all/', 'all');
$app->post('/users/create/', 'create');


?>