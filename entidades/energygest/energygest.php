<?php
	
	
	function getDatos(){
		$url = 'https://telegest.energygest.com/webservices/asesoria_4plus.php?id=8tG6iF7cAPQ7';
		$queryData = Functions::capturateJson($url);
		
		return $queryData["curvas"];
	}

	function insertarCuarto(){
		$array = getDatos();
	
		$table = 'energygest_cuarto11';
		$fields[] = 'Cups';
		$fields[] = 'Fecha_Hora';
		$fields[] = 'Activa_kWh';
		$fields[] = 'Reactiva_kVARh';

		//Separa todos los datos de curvas_cuatro. Para acceder usar $values[][]
		foreach ($array as $key => $value) {			
			foreach ($value['curvas_cuarto'] as $key2 => $value2) {
				$values[$key][$key2]['cups'] = $value['cups'];
				$values[$key][$key2]['fecha'] = $value2['fecha'];
				$values[$key][$key2]['activa'] = $value2['activa'];
				$values[$key][$key2]['reactiva'] = $value2['reactiva'];
			}
		}

		echo json_encode($values);
		//Functions::insert($table,$fields,$values);
		Functions::registry(Functions::current_url(),http_response_code(),__FUNCTION__);
	}

	function insertarHora(){
		$array = getDatos();

		$table = 'energygest_hora11';

		$fields[] = 'Cups';
		$fields[] = 'Fecha_Hora';
		$fields[] = 'Activa_kWh';
		$fields[] = 'Reactiva_kVARh';

		//Separa todos los datos de curvas_hora. Para acceder usar $values[][]
		foreach ($array as $key => $value) {			
			foreach ($value['curvas_hora'] as $key2 => $value2) {
				$values[$key][$key2]['cups'] = $value['cups'];
				$values[$key][$key2]['fecha'] = $value2['fecha'];
				$values[$key][$key2]['activa'] = $value2['activa'];
				$values[$key][$key2]['reactiva'] = $value2['reactiva'];
			}
		}


		echo json_encode($values);
		Functions::registry(Functions::current_url(),http_response_code(),__FUNCTION__);
	}

	$app->get('/energygest/getDatos/', 'getDatos');
	$app->get('/energygest/insertarCuarto/', 'insertarCuarto');
	$app->get('/energygest/insertarHora/', 'insertarHora');
?>