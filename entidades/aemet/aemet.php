<?php

/*
https://opendata.aemet.es/opendata/api/valores/climatologicos/diarios/datos/fechaini/2019-02-01T00:00:00UTC/fechafin/2019-02-28T23:59:59UTC/estacion/8309X/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlZmljaWVuY2lhMkBjdWF0cm9wbHVzLmNvbSIsImp0aSI6IjZjOGM1MjkxLWRlZTgtNDE5OC05MmM0LWQ3M2VlNThjNGU4NiIsImlzcyI6IkFFTUVUIiwiaWF0IjoxNTUwNTA2ODI4LCJ1c2VySWQiOiI2YzhjNTI5MS1kZWU4LTQxOTgtOTJjNC1kNzNlZTU4YzRlODYiLCJyb2xlIjoiIn0.Q77QnN8UYDz3Z0M0DuB5Q2NqTf2XV_5K7KLWTCidgyQ

*/
function allAemet()  {
		$fields[] = '*';
		$table = 'aemet';
		$fieldsW[] = "1";
		$values[] = "1";
		$rows = Functions::records($fields, $table, "", $fieldsW, $values, 'FETCH_ASSOC');
   		Functions::generateJson($rows);
}


function createAemet()  {

	$queryData['estado'] = 0;	
	$queryData['descripcion'] = "La consulta no está siendo correcta";	

	$currentDate = date("Y-m-d");//"2019-03-01";
	$currentTime = date("H:i:s");
	$queryData = ("https://opendata.aemet.es/opendata/api/valores/climatologicos/diarios/datos/fechaini/".$currentDate."T00:00:00UTC/fechafin/".$currentDate."T23:59:59UTC/estacion/8309X/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlZmljaWVuY2lhMkBjdWF0cm9wbHVzLmNvbSIsImp0aSI6IjZjOGM1MjkxLWRlZTgtNDE5OC05MmM0LWQ3M2VlNThjNGU4NiIsImlzcyI6IkFFTUVUIiwiaWF0IjoxNTUwNTA2ODI4LCJ1c2VySWQiOiI2YzhjNTI5MS1kZWU4LTQxOTgtOTJjNC1kNzNlZTU4YzRlODYiLCJyb2xlIjoiIn0.Q77QnN8UYDz3Z0M0DuB5Q2NqTf2XV_5K7KLWTCidgyQ");
	
$queryData = Functions::capturateJson($queryData); 
$table = 'aemet';

if($queryData['estado'] == 200)
{
	$urlData = $queryData['datos'];
	$dataResult = Functions::capturateJson($urlData); 
	
	$fields[] = 'estado';
	$fields[] = 'descripcion';
	$fields[] = 'fecha';
	$fields[] = 'indicativo';
	$fields[] = 'nombre';
	$fields[] = 'provincia';
	$fields[] = 'altitud';
	$fields[] = 'tmed';
	$fields[] = 'prec';
	$fields[] = 'tmin';
	$fields[] = 'horatmin';
	$fields[] = 'tmax';
	$fields[] = 'horatmax';
	$fields[] = 'dir';
	$fields[] = 'velmedia';
	$fields[] = 'racha';
	$fields[] = 'horaracha';
	$fields[] = 'sol';
	$fields[] = 'presMax';
	$fields[] = 'horaPresMax';
	$fields[] = 'presMin';
	$fields[] = 'horaPresMin';
	
	$values[] = $queryData['estado'];
	$values[] = $queryData['descripcion'];
	$values[] = $dataResult[0]['fecha'];
	$values[] = $dataResult[0]['indicativo'];
	$values[] = $dataResult[0]['nombre'];
	$values[] = $dataResult[0]['provincia'];
	$values[] = $dataResult[0]['altitud'];
	$values[] = $dataResult[0]['tmed'];
	$values[] = $dataResult[0]['prec'];
	$values[] = $dataResult[0]['tmin'];
	$values[] = $dataResult[0]['horatmin'];
	$values[] = $dataResult[0]['tmax'];
	$values[] = $dataResult[0]['horatmax'];
	$values[] = $dataResult[0]['dir'];
	$values[] = $dataResult[0]['velmedia'];
	$values[] = $dataResult[0]['racha'];
	$values[] = $dataResult[0]['horaracha'];
	$values[] = $dataResult[0]['sol'];
	$values[] = $dataResult[0]['presMax'];
	$values[] = $dataResult[0]['horaPresMax'];
	$values[] = $dataResult[0]['presMin'];
	$values[] = $dataResult[0]['horaPresMin'];
	$result["msg"] = "Consulta exitosa";
	
	$result["result"] = Functions::insert($table, $fields, $values);
	
}else
{
	$fields[] = 'estado';
	$fields[] = 'descripcion';
	
	
	$values[] = $queryData['estado'];
	$values[] = $queryData['descripcion'];	
	$result["msg"] = "Error en la consulta";
	$result["result"] = Functions::insert($table, $fields, $values);
	
}

Functions::generateJson($result);
} 


$app->post('/aemet/all/', 'allAemet');
$app->post('/aemet/create/', 'createAemet');

?>
